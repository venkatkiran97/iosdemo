//
//  IDViewController.swift
//  iOSDemo
//
//  Created by Dommeti Venkata Durga Kiran . on 10/08/19.
//  Copyright © 2019 Dommeti Venkata Durga Kiran . All rights reserved.
//

import UIKit

class IDViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
    }
 }
