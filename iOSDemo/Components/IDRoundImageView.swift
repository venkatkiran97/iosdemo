//
//  IDRoundImageView.swift
//  iOSDemo
//
//  Created by Dommeti Venkata Durga Kiran . on 09/08/19.
//  Copyright © 2019 Dommeti Venkata Durga Kiran . All rights reserved.
//

import UIKit

class IDRoundImageView: UIImageView {
    
    override func awakeFromNib() {
        self.layer.cornerRadius = self.frame.size.height * 0.5
        self.layer.masksToBounds = true
        self.clipsToBounds = true
    }
}
