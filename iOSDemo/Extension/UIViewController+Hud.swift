//
//  UIViewController+Hud.swift
//  iOSDemo
//
//  Created by Dommeti Venkata Durga Kiran . on 09/08/19.
//  Copyright © 2019 Dommeti Venkata Durga Kiran . All rights reserved.
//

import UIKit
import MBProgressHUD

extension UIViewController {
    func showHUD(progressLabel:String){
        DispatchQueue.main.async{
            let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
            progressHUD.label.text = progressLabel
        }
    }
    
    func dismissHUD(isAnimated:Bool) {
        DispatchQueue.main.async{
            MBProgressHUD.hide(for: self.view, animated: isAnimated)
        }
    }
    
    func showToastMessage(message: String) {
        DispatchQueue.main.async{
            let progressHUD =  MBProgressHUD.showAdded(to: self.navigationController!.view, animated: true)
            progressHUD.mode = .text
            progressHUD.label.text = message
            progressHUD.offset = CGPoint(x: 0, y: MBProgressMaxOffset)
            progressHUD.hide(animated: true, afterDelay: 3)
        }
    }
}



