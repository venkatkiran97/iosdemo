//
//  Utilities.swift
//  iOSDemo
//
//  Created by Dommeti Venkata Durga Kiran . on 09/08/19.
//  Copyright © 2019 Dommeti Venkata Durga Kiran . All rights reserved.
//

import UIKit

class Utilities {
    class func getURLWithSubKey(key: String) -> String? {
        let plistPath: String? = Bundle.main.path(forResource:"URIConfig", ofType: "plist")
        let uriConfigCache: NSDictionary? = plistPath != nil ? NSDictionary(contentsOfFile: plistPath!) : nil
        return uriConfigCache?.value(forKey: key) as? String
    }
    
    
    class func getURLWithKey(key: String) -> String? {
        let plistPath: String? = Bundle.main.path(forResource:"URIConfig", ofType: "plist")
        let uriConfigCache: NSDictionary? = plistPath != nil ? NSDictionary(contentsOfFile: plistPath!) : nil
        let baseURL = uriConfigCache?.value(forKey: "baseURL") as? String
        let serviceURL = getURLWithSubKey(key: key)
        if let baseURI = baseURL , let serviceURI = serviceURL{
            return baseURI + serviceURI
        }
        return ""
    }
    
    class func getImageBaseURLWithKey(imageName: String) -> String? {
        let plistPath: String? = Bundle.main.path(forResource:"URIConfig", ofType: "plist")
        let uriConfigCache: NSDictionary? = plistPath != nil ? NSDictionary(contentsOfFile: plistPath!) : nil
        let baseURL = uriConfigCache?.value(forKey: "imageBaseUrl") as? String
        let serviceURL = getURLWithSubKey(key: imageName)
        if let baseURI = baseURL , let serviceURI = serviceURL{
            return baseURI + serviceURI
        }
        return ""
    }
    
    
    
  class func getAuthToken() -> String? {
        let userInfo = UserDefaults.standard.decode(for: User.self, using: KeyConstants.userInfoKey)
        if let authToken = userInfo?.apiToken {
            return authToken
        }
        return nil
        
    }

}


