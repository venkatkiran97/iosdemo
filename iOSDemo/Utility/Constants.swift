//
//  Constants.swift
//  iOSDemo
//
//  Created by Dommeti Venkata Durga Kiran . on 09/08/19.
//  Copyright © 2019 Dommeti Venkata Durga Kiran . All rights reserved.
//

import UIKit

public struct ImageURLConstants{
    public static let imageBaseUrl = "https://cdn.vox-cdn.com/uploads/chorus_image/image/"
    public static let imageChorusAssetBaseUrl = "https://cdn.vox-cdn.com/uploads/chorus_asset/file/"
}

public struct StringConstants{
    public static let logging = "Logging In..."
    public static let noInternet = "No Internet Connection"
    public static let registering = "Registering..."
    public static let fetching = "Fetching..."
    public static let genderSelect = "Please select your gender."
    public static let male = "Male"
    public static let female = "Female"
    public static let cancel = "Cancel"
    public static let logoutMessage = "Are you sure you want to logout?"
    public static let tryAgainMessage = "Please try again later."
    public static let yes = "Yes"
    public static let no = "No"
    
}

public struct ValidationConstants{
    public static let emailEmpty = "Please enter your email."
   public static let emailValidation =  "Please enter valid email."
    public static let passwordEmpty = "Please enter your password."
   public static let usernameEmpty = "Please enter your name."
   public static let confirmPasswordEmpty = "Please confirm your password."
   public static let mobileEmpty = "Please enter your mobile."
   public static let genderEmpty = "Please select your gender."
    public static let mobileValidation = "Please enter valid mobile."
    public static let passwordMatchValidation = "Passwords don't Match"
}

public struct KeyConstants{
    public static let userInfoKey = "UserInfo"
}

public struct TableCellConstants{
   public static let newsfeedTableCell = "NewsFeedTableCellTableViewCell"
    public static let newsFeedTableIdentifier = "NewsFeedTableCellTableViewCellIdentifier"
}
