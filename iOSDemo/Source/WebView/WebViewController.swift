//
//  WebViewController.swift
//  iOSDemo
//
//  Created by Dommeti Venkata Durga Kiran . on 10/08/19.
//  Copyright © 2019 Dommeti Venkata Durga Kiran . All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController {
    
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var urlString = ""
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicator.stopAnimating()
        webView.navigationDelegate = self
        let url = URL(string: urlString)!
        webView.load(URLRequest(url: url)) // loading url
        webView.allowsBackForwardNavigationGestures = true
        // Do any additional setup after loading the view.
    }
    
}

/**
 * WebKit Delegate Methods
 */
extension WebViewController: WKNavigationDelegate{
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        activityIndicator.stopAnimating()
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        activityIndicator.startAnimating()
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        activityIndicator.stopAnimating()
    }
}
