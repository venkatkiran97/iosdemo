//
//  ViewController.swift
//  iOSDemo
//
//  Created by Dommeti Venkata Durga Kiran . on 09/08/19.
//  Copyright © 2019 Dommeti Venkata Durga Kiran . All rights reserved.
//

import UIKit
import MBProgressHUD

class ViewController: IDViewController {
    
    @IBOutlet weak var emailTextField: IDTextField!
    @IBOutlet weak var passwordTextField: IDTextField!
    @IBOutlet weak var loginBtn: IDButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    /**
     * Calls when user tapped on Login Button
     */
    @IBAction func loginBtnTapped(_ sender: IDButton) {
        guard let email = emailTextField.text, emailTextField.text?.count != 0 else {
            //If user doesn't enter anything in email text field
            self.showToastMessage(message: ValidationConstants.emailEmpty)
            return
        }
        
        if email.isValidEmail() == false{
            //Email Validation False
            self.showToastMessage(message: ValidationConstants.emailValidation)
            return
        }
        
        guard let password = passwordTextField.text, passwordTextField.text?.count != 0 else {
            //If user doesn't enter anything in password text field
            self.showToastMessage(message: ValidationConstants.passwordEmpty)
            return
        }
        
        if Reachability.isConnectedToNetwork(){
            //If device has internet
            self.showHUD(progressLabel: StringConstants.logging)
            LoginManager.loginAPI(email: email, password: password) { (result) in
                self.dismissHUD(isAnimated: true)
                if result.success == false {
                    //If email is not registered
                    self.showToastMessage(message: result.message!)
                }
                //Saving UserInfo in Userdefaults
                UserDefaults.standard.encode(for: result.user, using: KeyConstants.userInfoKey)
                self.navigationController?.pushViewController(NewsFeedViewController(), animated: true)
            }
        }else{
            //If Internet connection is not active
            self.showToastMessage(message: StringConstants.noInternet)
        }
    }
    
    /**
     * Calls when user wants to create an account
     */
    @IBAction func createAccountTapped(_ sender: Any) {
        self.navigationController?.pushViewController(SignUpViewController(), animated: true)
    }
}




