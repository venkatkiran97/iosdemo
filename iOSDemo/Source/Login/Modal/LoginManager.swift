//
//  LoginManager.swift
//  iOSDemo
//
//  Created by Dommeti Venkata Durga Kiran . on 09/08/19.
//  Copyright © 2019 Dommeti Venkata Durga Kiran . All rights reserved.
//

import UIKit
import Alamofire

class LoginManager {
   static func loginAPI(email: String!, password: String!, completion: @escaping(LoginData) -> Void) {
    
    let parameters : Parameters = [
        "email":email,
        "password":password
    ]
    
    print(parameters)
    
    Alamofire.request(Utilities.getURLWithKey(key: "login")!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
        
        if let JSONString = String(data: response.data!, encoding: String.Encoding.utf8) {
            print(JSONString)
        }
            switch response.result{
                case  .success(_):
                    do{
                        let decoder = JSONDecoder()
                        //using the array to put values
                        let loginResponse = try decoder.decode(LoginData.self, from: response.data!)
                        completion(loginResponse)
                        
                    }catch let err{
                        print(err)
                }
                
                
            case let .failure(error):
                print(error)
            }
        }
    }
}
