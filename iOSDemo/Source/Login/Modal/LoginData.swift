//
//  LoginData.swift
//  iOSDemo
//
//  Created by Dommeti Venkata Durga Kiran . on 09/08/19.
//  Copyright © 2019 Dommeti Venkata Durga Kiran . All rights reserved.
//

//   let welcome = try? newJSONDecoder().decode(Welcome.self, from: jsonData)

import Foundation

// MARK: - Welcome
struct LoginData: Codable {
    let status, message: String?
    let user: User?
    let success : Bool?
    
}

// MARK: - User
struct User: Codable {
    let id: Int
    let name, email, apiToken, rateLimit: String
    let walletBalance, facebook, google, twitter: Int
    let userType: String
    
    enum CodingKeys: String, CodingKey {
        case id, name, email
        case apiToken = "api_token"
        case rateLimit = "rate_limit"
        case walletBalance = "wallet_balance"
        case facebook, google, twitter
        case userType = "user_type"
    }
}
