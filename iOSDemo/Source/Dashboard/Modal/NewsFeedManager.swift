//
//  NewsFeedManager.swift
//  iOSDemo
//
//  Created by Dommeti Venkata Durga Kiran . on 09/08/19.
//  Copyright © 2019 Dommeti Venkata Durga Kiran . All rights reserved.
//

import UIKit
import Alamofire

class NewsFeedManager {
    static func NewsFeedAPI(pageNumber: Int ,completion: @escaping(NewsFeedData) -> Void) {

        var authToken = ""
        if let auth = Utilities.getAuthToken(){
            authToken = auth
        }
        
            let headers: HTTPHeaders = [
                "Authorization": "Bearer " + authToken,
                "Accept": "application/json"
            ]
        
        Alamofire.request(Utilities.getURLWithKey(key: "kStream")! + String(pageNumber), method: .get, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            
            if let JSONString = String(data: response.data!, encoding: String.Encoding.utf8) {
                print(JSONString)
            }
            switch response.result{
            case  .success(_):
                do{
                    let decoder = JSONDecoder()
                    let loginResponse = try decoder.decode(NewsFeedData.self, from: response.data!)
                    completion(loginResponse)
                    
                }catch let err{
                    print(err)
                }
                
            case let .failure(error):
                print(error)
            }
        }
    }
}
