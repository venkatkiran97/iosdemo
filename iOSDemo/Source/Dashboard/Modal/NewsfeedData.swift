//
//  NewsfeedData.swift
//  iOSDemo
//
//  Created by Dommeti Venkata Durga Kiran . on 09/08/19.
//  Copyright © 2019 Dommeti Venkata Durga Kiran . All rights reserved.
//


import Foundation

// MARK: - NewsFeedData
struct NewsFeedData: Codable {
    let success: Bool
    let kstream: Kstream?
}

// MARK: - Kstream
struct Kstream: Codable {
    let currentPage: Int
    let data: [NewsFeed]?
    let firstPageURL: String?
    let from: Int
    let nextPageURL, path: String
    let perPage: Int?
    let prevPageURL: String?
    let to: Int
    
    enum CodingKeys: String, CodingKey {
        case currentPage = "current_page"
        case data
        case firstPageURL = "first_page_url"
        case from
        case nextPageURL = "next_page_url"
        case path
        case perPage = "per_page"
        case prevPageURL = "prev_page_url"
        case to
    }
}

// MARK: - Datum
struct NewsFeed: Codable {
    let id, rssSourceID: Int
    let title: String
    let titleImage, tagLine, shortDescription: String?
    let fullDescription: String
    let titleImageURL: String?
    let descriptionImageURL: String?
    let articleURL: String?
    let articleType: ArticleType
    let publishedDate: String
    let isSponsored, isPremium: Int
    let tags, filtertags: String?
    let likes, comments, shares, metaKstreamID: Int
    let accepted: Int
    let createdAt: String?
    let updatedAt: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case rssSourceID = "rss_source_id"
        case title
        case titleImage = "title_image"
        case tagLine = "tag_line"
        case shortDescription = "short_description"
        case fullDescription = "full_description"
        case titleImageURL = "title_image_url"
        case descriptionImageURL = "description_image_url"
        case articleURL = "article_url"
        case articleType = "article_type"
        case publishedDate = "published_date"
        case isSponsored = "is_sponsored"
        case isPremium = "is_premium"
        case tags, filtertags, likes, comments, shares
        case metaKstreamID = "meta_kstream_id"
        case accepted
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
}

enum ArticleType: String, Codable {
    case blog = "BLOG"
    case technology = "Technology"
}
