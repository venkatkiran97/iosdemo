//
//  NewsFeedViewController.swift
//  iOSDemo
//
//  Created by Dommeti Venkata Durga Kiran . on 09/08/19.
//  Copyright © 2019 Dommeti Venkata Durga Kiran . All rights reserved.
//

import UIKit

class NewsFeedViewController: IDViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var kStream : Kstream?
    var newsfeedData = [NewsFeed]()
    var currentPage  = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpTableView()
        newsFeedAPI(page: currentPage) // Calling an API for kStream page 1
    }
    
    /**
     * Calls when user tapped on Log Out Button
     */
    @IBAction func logOutBtnTapped(_ sender: UIButton) {
        let alert = UIAlertController(title: nil, message: StringConstants.logoutMessage, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: StringConstants.yes, style: .default, handler: { (action) in
            UserDefaults.standard.removeObject(forKey: KeyConstants.userInfoKey)
            UserDefaults.standard.synchronize()
            self.navigationController?.popToRootViewController(animated: true)
        }))
        
        alert.addAction(UIAlertAction(title: StringConstants.no, style: .destructive, handler: nil))
        self.present(alert,animated: true, completion: nil)
    }
    
    /**
     * Setting Table View with Registering cells
     */
    func setUpTableView() {
        tableView.register(UINib(nibName: TableCellConstants.newsfeedTableCell, bundle: nil), forCellReuseIdentifier: TableCellConstants.newsFeedTableIdentifier)
        tableView.estimatedRowHeight = 200
        tableView.rowHeight = UITableView.automaticDimension
    }
    
    /**
     * Calling Newsfeed data with pagination
     */
    func newsFeedAPI(page: Int) {
        if(Reachability.isConnectedToNetwork()){
            self.showHUD(progressLabel: StringConstants.fetching)
            NewsFeedManager.NewsFeedAPI(pageNumber: page) { (response) in
                self.dismissHUD(isAnimated: true)
                if let feedResponse = response.kstream{
                    self.kStream = feedResponse
                    if let feedArray = feedResponse.data{
                        self.newsfeedData.append(contentsOf: feedArray)
                    }
                    DispatchQueue.main.async {
                        //Rendering data into table view
                        self.tableView.reloadData()
                    }
                }else{
                    self.showToastMessage(message: StringConstants.tryAgainMessage)
                }
            }
        }else{
            self.showToastMessage(message: StringConstants.noInternet)
        }
       
    }
}

/**
 * Table View Delegate Methods
 */
extension NewsFeedViewController : UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

/**
 * Table View DataSource Methods
 */
extension NewsFeedViewController : UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newsfeedData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewsFeedTableCellTableViewCellIdentifier", for: indexPath) as! NewsFeedTableCellTableViewCell
        cell.delegate = self
        cell.newsFeed = newsfeedData[indexPath.row] // transfer newsfeeddata to cell
        if indexPath.row == newsfeedData.count - 1 {
            //Validating for last cell
            if let _ = kStream?.nextPageURL {
                currentPage += 1
                newsFeedAPI(page: currentPage)
            }
        }
        return cell
    }
}

/**
 * NewsFeed Protocol
 */
extension NewsFeedViewController : NewsFeedCellButtonProtocol{
    func likeBtnTapped(likeCount: String) {
        // TODO: Not getting time to work on this
    }
    
    /**
     * Calls When See More Btn Tapped
     */
    func seeMoreBtnTapped(cell: NewsFeedTableCellTableViewCell) {
        guard let indexPath = self.tableView.indexPath(for: cell) else {
            return
        }
        
        if let articleURL = newsfeedData[indexPath.row].articleURL{
            //Navigating to webView
            let webVC = WebViewController()
            webVC.urlString = articleURL
            self.navigationController?.pushViewController(webVC, animated: true)
        }
        
    }
}
