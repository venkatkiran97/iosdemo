//
//  NewsFeedTableCellTableViewCell.swift
//  iOSDemo
//
//  Created by Dommeti Venkata Durga Kiran . on 09/08/19.
//  Copyright © 2019 Dommeti Venkata Durga Kiran . All rights reserved.
//

import UIKit
//import Kingfisher
import SDWebImage

protocol NewsFeedCellButtonProtocol {
    func likeBtnTapped(likeCount: String)
    func seeMoreBtnTapped(cell: NewsFeedTableCellTableViewCell) 
}

class NewsFeedTableCellTableViewCell: UITableViewCell {
    
    @IBOutlet weak var readMoreBtn: UIButton!
    var delegate : NewsFeedCellButtonProtocol?
    
    @IBOutlet weak var titleImageView: IDRoundImageView!
    @IBOutlet weak var titleLabel: IDHeaderLabel!
    @IBOutlet weak var descriptionLabel: IDLabel!
    @IBOutlet weak var descriptionImageView: UIImageView!
    @IBOutlet weak var likeBtn: UIImageView!
    @IBOutlet weak var likeCount: UILabel!
    @IBOutlet weak var commentCount: UILabel!
    @IBOutlet weak var shareCount: UILabel!
    
    var isExpanded = false
    
    var newsFeed : NewsFeed? {
        didSet{
            if let title = newsFeed?.title {
                titleLabel.text = title
            }
            if let description = newsFeed?.shortDescription {
                descriptionLabel.text = description
            }else{
                descriptionLabel.text = newsFeed?.fullDescription
            }
            
            if let likes = newsFeed?.likes {
                likeCount.text = String(likes)
            }
            
            if let comments = newsFeed?.comments {
                commentCount.text = String(comments)
            }
            
            if let shares = newsFeed?.shares {
                shareCount.text = String(shares)
            }
            
            if let titleImageUrl = newsFeed?.titleImageURL{
                let url : URL?
                if titleImageUrl.contains("cdn.vox-cdn.com") {
                    let imageArray = titleImageUrl.components(separatedBy: "/")
                    let count = imageArray.count
                    let last2 = imageArray[count - 2 ..< count]
                    let imageName = last2.joined(separator: "/")
                    
                    var imageUrl = ImageURLConstants.imageBaseUrl + imageName
                    if titleImageUrl.contains("chorus_asset/file/"){
                        imageUrl = ImageURLConstants.imageChorusAssetBaseUrl + imageName
                    }
                    url =  URL(string: imageUrl)
                } else{
                    url = URL(string: titleImageUrl)
                }
                
                titleImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder.png"))
            }
            
            if let descriptionImageUrl = newsFeed?.descriptionImageURL{
                let imageArray = descriptionImageUrl.components(separatedBy: "/")
                let count = imageArray.count
                let last2 = imageArray[count - 2 ..< count]
                let imageName = last2.joined(separator: "/")
                var url = ImageURLConstants.imageBaseUrl + imageName
                if descriptionImageUrl.contains("chorus_asset/file/"){
                    url = ImageURLConstants.imageChorusAssetBaseUrl + imageName
                }
                descriptionImageView.sd_setImage(with:URL(string: url), placeholderImage: UIImage(named: "placeholder.png"))
            }
            
        }
    }
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(likeBtnTapped(tapGestureRecognizer:)))
        likeBtn?.isUserInteractionEnabled = true
        likeBtn.addGestureRecognizer(tapGestureRecognizer)
        
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    @objc func likeBtnTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        delegate?.likeBtnTapped(likeCount: "5")
    }
    
    
    @IBAction func readMoreBtnTapped(_ sender: Any) {
        delegate?.seeMoreBtnTapped(cell: self)
    }
    
}
