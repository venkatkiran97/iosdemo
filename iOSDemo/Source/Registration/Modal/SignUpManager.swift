//
//  SignUpManager.swift
//  iOSDemo
//
//  Created by Dommeti Venkata Durga Kiran . on 09/08/19.
//  Copyright © 2019 Dommeti Venkata Durga Kiran . All rights reserved.
//

import UIKit
import Alamofire

class SignUpManager {
    static func SignUpAPI(parameters: Parameters, completion: @escaping(SignUpData) -> Void) {
        
        print(parameters)
        
        Alamofire.request(Utilities.getURLWithKey(key: "register")!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            
            if let JSONString = String(data: response.data!, encoding: String.Encoding.utf8) {
                print(JSONString)
            }
            switch response.result{
            case  .success(_):
                do{
                    let decoder = JSONDecoder()
                    let loginResponse = try decoder.decode(SignUpData.self, from: response.data!)
                    completion(loginResponse)
                    
                }catch let err{
                    print(err)
                }
                
                
            case let .failure(error):
                print(error)
            }
        }
    }
}
