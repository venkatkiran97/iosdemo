//
//  SignUpData.swift
//  iOSDemo
//
//  Created by Dommeti Venkata Durga Kiran . on 09/08/19.
//  Copyright © 2019 Dommeti Venkata Durga Kiran . All rights reserved.
//

import UIKit

struct SignUpData: Codable {
    let success: Bool?
    let message: String?
}
