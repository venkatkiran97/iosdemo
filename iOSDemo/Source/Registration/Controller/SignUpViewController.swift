//
//  SignUpViewController.swift
//  iOSDemo
//
//  Created by Dommeti Venkata Durga Kiran . on 09/08/19.
//  Copyright © 2019 Dommeti Venkata Durga Kiran . All rights reserved.
//

import UIKit
import Alamofire

class SignUpViewController: IDViewController {
    
    
    @IBOutlet weak var usernameTextField: IDTextField!
    @IBOutlet weak var emailTextField: IDTextField!
    @IBOutlet weak var passwordTextField: IDTextField!
    @IBOutlet weak var confirmPasswordTxtField: IDTextField!
    @IBOutlet weak var mobileTextField: IDTextField!
    @IBOutlet weak var genderTextField: IDTextField!
    
    @IBOutlet weak var signUpBtn: IDButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    /**
     * Calls when user tapped on Back Button
     */
    @IBAction func backBtnTapped(_ sender: Any) {
        navigationController?.popToRootViewController(animated: true)
    }
    
    /**
     * Calls when user tapped on Sign Up Button
     */
    @IBAction func signUpBtnTapped(_ sender: Any) {
        guard let username = usernameTextField.text, usernameTextField.text?.count != 0 else {
            //If user doesn't enter anything in username text field
            self.showToastMessage(message: ValidationConstants.usernameEmpty)
            return
        }
        
        guard let email = emailTextField.text, emailTextField.text?.count != 0 else {
            //If user doesn't enter anything in email text field
            self.showToastMessage(message: ValidationConstants.emailEmpty)
            return
        }
        
        guard let password = passwordTextField.text, passwordTextField.text?.count != 0 else {
            //If user doesn't enter anything in password text field
            self.showToastMessage(message: ValidationConstants.passwordEmpty)
            return
        }
        
        guard let confirmPassword = confirmPasswordTxtField.text, confirmPasswordTxtField.text?.count != 0 else {
            //If user doesn't enter anything in confirm password text field
            self.showToastMessage(message: ValidationConstants.confirmPasswordEmpty)
            return
        }
        
        guard let mobile = mobileTextField.text, mobileTextField.text?.count != 0 else {
            //If user doesn't enter anything in mobile text field
            self.showToastMessage(message: ValidationConstants.mobileEmpty)
            return
        }
        
        guard let gender = genderTextField.text, genderTextField.text?.count != 0 else {
            //If user doesn't select anything in gender
            self.showToastMessage(message: ValidationConstants.genderEmpty)
            return
        }
        
        if (email.isValidEmail() == false)
        {
            //Email Validation False
            self.showToastMessage(message: ValidationConstants.emailValidation)
            return
        }
        
        if (mobile.isValidContact == false)
        {
            //Mobile Validation False
            self.showToastMessage(message: ValidationConstants.mobileValidation)
            return
        }
        
        if password != confirmPassword {
            //Password doesn't match
            self.showToastMessage(message: ValidationConstants.passwordMatchValidation)
        }
        
        if Reachability.isConnectedToNetwork(){
           //If device has internet connection
            self.showHUD(progressLabel: StringConstants.registering)
            let parameter  = getParameters(username: username, email: email, password: password, confirmPassword: confirmPassword, mobile: mobile, gender: gender)
            SignUpManager.SignUpAPI(parameters: parameter) { (response) in
                self.dismissHUD(isAnimated: true)
                if response.success == false {
                    // If regisration fails
                    self.showToastMessage(message: response.message!)
                }else{
                    //If successfully registered
                    self.navigationController?.pushViewController(NewsFeedViewController(), animated: true)
                }
            }
        }else{
            self.showToastMessage(message: StringConstants.noInternet)
        }
    }
    
    /**
     * Adding body parameters for Register API call
     */
    func getParameters(username: String, email: String, password: String, confirmPassword: String, mobile: String, gender: String ) -> Parameters{
        // Getting the parameters to pass in API
        let param:Parameters = [
            "name":username,
            "email":email,
            "password":password,
            "password_confirmation":confirmPassword,
            "mobile":mobile,
            "gender":gender
        ]
        return param
    }
}


/**
 * TextField Delegate
 */
extension SignUpViewController: UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == genderTextField{
            //Open Actionsheet for selection
            textField.resignFirstResponder()
            let alert = UIAlertController(title: "", message: StringConstants.genderSelect, preferredStyle: .actionSheet)
            
            alert.addAction(UIAlertAction(title: StringConstants.male, style: .default , handler:{ (UIAlertAction)in
                //If user selects Male
                textField.text = StringConstants.male
                self.view.endEditing(true)
            }))
            
            
            alert.addAction(UIAlertAction(title: StringConstants.female, style: .default , handler:{ (UIAlertAction)in
                //If user selects Female
                textField.text = StringConstants.female
                self.view.endEditing(true)
            }))
            
            alert.addAction(UIAlertAction(title: StringConstants.cancel, style: .destructive, handler: { (UIAlertAction) in
                //If user selects cancel
                textField.resignFirstResponder()
            }))
            
            self.present(alert, animated: true, completion: {
                
            })
            return false
        }
        return false
    }
}
